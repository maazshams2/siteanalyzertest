# siteAnalyzerTest

# Pre-Requisite:
- Selenium WebDriver,
Install here: https://www.seleniumhq.org/download/

- NodeJS,
Install here: https://nodejs.org/en/download/

- IDE of JS (WebStorm, VS Code etc)


# Protractor Installation:
a. After downloading the pre-requisite, open cmd in the respective directory & type following command and hit Enter.

```npm i protractor```

b. Update and Start the web driver manager by executing following command.

```webdriver-manager update```, 
```webdriver-manager start```

c. By executing following command, all the dependencies in *package.json* will be download locally

```npm i```



# Code Execution:
To execute the code of protractor, open cmd in the directory, navigate to /app & type following:
```protractor protractor-conf.js```

*protractor-conf.js* is the configuration file having address & specs test file


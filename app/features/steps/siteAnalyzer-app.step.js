var _ = require('lodash');
var siteAnalyzerPage = require('../../pages/siteAnalyzer-page')
var siteResultPage = require('../../pages/siteResult-page')

module.exports = function () {
    this.Given('I\'m on Site Analyzer page', ()=> {
        browser.get(browser.params.urls.base);
    });
    
    this.When('Enter correct URL in search bar', function () {
        siteAnalyzerPage.searchUrl(browser.params.urls.correct);
        siteAnalyzerPage.clickSearchButton();
    });
    this.When('Enter incorrect URL in search bar', function () {
        siteAnalyzerPage.searchUrl(browser.params.urls.incorrect);
        siteAnalyzerPage.clickSearchButton();
    });
    this.When('Enter incorrect format URL in search bar', function () {
        siteAnalyzerPage.searchUrl(browser.params.urls.incorrectFormat);
        siteAnalyzerPage.clickSearchButton();
    });
    // for dynamic url handling
    // not working, will look into it later
    // this.When('Enter {string} URL in search bar', (url) => {
    this.When('Enter "([^"]*)" URL in search bar', function (url) {
        console.log(url);
        // siteAnalyzerPage.searchUrl(url);
    });

    this.Then('Validate unsuccessful result', (dataTable)=> {
        siteAnalyzerPage.validateCardTitle('Error!');
        var expectations = dataTable.hashes();
        for (let i=0 ; i<expectations.length ; i++) {
            siteAnalyzerPage.validateCardContent(expectations[i].description);
            siteAnalyzerPage.validateCardFooter(expectations[i].statusCode);
        }
    });

    this.Then('Validate result', (dataTable)=> {
        var expectations = dataTable.hashes();

        siteAnalyzerPage.validateCardTitle('Analyze Results');

        //For each row you will get the user and the country
        for (let i=0 ; i<expectations.length ; i++) {
            siteResultPage.verifyDocVersion(expectations[i].version);
            siteResultPage.verifyPageTitle(expectations[i].title);
            siteResultPage.verifyHasLoginForm(expectations[i].loginForm);

            var assertArray = [expectations[i].h1, expectations[i].h2, expectations[i].h3, expectations[i].h4, expectations[i].h5,  
                expectations[i].h6, expectations[i].intLinks, expectations[i].extLinks, expectations[i].inaccesibleIntLinks, 
                expectations[i].inaccesibleExtLinks];
            siteResultPage.verifyBadges(assertArray);
        };
    })
}

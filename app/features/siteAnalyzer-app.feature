Feature: Protractor and cucumber Site Analyzer
  Scenario: Successfully searched given url
    Given I'm on Site Analyzer page
    When Enter correct URL in search bar
    # When Enter "https://github.com/ashwinkumar01" URL in search bar
    Then Validate result
| version |                title                  | h1 | h2 | h3 | h4 | h5 | h6 | intLinks | extLinks | inaccesibleIntLinks | inaccesibleExtLinks | loginForm |
|  HTML5  | ashwinkumar01 (Ashwin Kumar) · GitHub | 1  |  3 | 4  | 2  | 1  | 0  |    54    |    29    |          0          |          0          |     No    |

  Scenario: Wrong url searched
    Given I'm on Site Analyzer page
    When Enter incorrect URL in search bar
    # When Enter "https://wrongsite.foo" URL in search bar
    Then Validate unsuccessful result
|               description             |        statusCode         |
|  getaddrinfo ENOTFOUND wrongsite.foo  | 500 Internal Server Error |

  Scenario: Incorrect formatted url searched
    Given I'm on Site Analyzer page
    When Enter incorrect format URL in search bar
    # When Enter "asdasd" URL in search bar
    Then Validate unsuccessful result
|                      description                   |   statusCode    |
|  Site name must be typed with proper http prefix!  | 400 Bad Request |


'use strict';

exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',

    specs : [
        'features/*.feature'    // require feature files
    ],

    capabilities: {
        browserName: 'chrome',
        'goog:chromeOptions': {
            args: [ 
                /*"--headless", */
                "--start-maximized" 
            ]
        }
    },

    scripts: {
        start: "protractor protractor-conf.js"
    },

    params: {
        urls: {
          base: 'http://localhost:4200',
          correct: 'https://github.com/ashwinkumar01',
          incorrect: 'https://wrongsite.foo',
          incorrectFormat: 'asdasd'
        }},

    framework : 'custom',   // set to "custom" instead of cucumber.
    frameworkPath: require.resolve('protractor-cucumber-framework'),    // path relative to the current config file
    directConnection : true,

    cucumberOpts: {
        require: '**/**.js'  // require step definitions
    },

    onPrepare() {
        browser.waitForAngularEnabled(false);
    },
    
    onComplete() {
    browser.driver.close();
    }
}
var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
var expect = chai.expect;

const docVersion = element.all(by.css('.mat-chip')).first();
const pageTitle = element.all(by.css('.mat-chip')).get(1);
const hasLoginForm = element.all(by.css('.mat-chip')).last();
const badges = element.all(by.css('.mat-badge-active'));
var EC = protractor.ExpectedConditions;

exports.verifyDocVersion = (result) => {
    docVersion.getText().then(function(text){
        expect(text).to.equal(result);
    });
}
exports.verifyPageTitle = (result) => {
    pageTitle.getText().then(function(text){
        expect(text).to.equal(result);
    });
}
exports.verifyHasLoginForm = (result) => {
    hasLoginForm.getText().then(function(text){
        expect(text).to.equal(result);
    });
}
exports.verifyBadges = (array) => {
    for (let i=0 ; i<badges.length ; i++) {
        const element = badges[i];

        element.getText().then(function(text){
            console.log("Badge Text:   " + text);
            expect(text).to.equal(array[i]);
        });
    }
}

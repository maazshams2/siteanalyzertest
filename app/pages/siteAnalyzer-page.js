var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
var expect = chai.expect;

const urlSearchField = element(by.id('mat-input-0'));
const searchButton = element(by.css('.mat-button-base'));
const cardTitle = element(by.css('.mat-card-title'));
const cardContent = element(by.css('.mat-card-content'));
const cardFooter = element(by.css('.mat-card-footer'));
var EC = protractor.ExpectedConditions;

exports.searchUrl = (data) => {
    urlSearchField.sendKeys(data);
}

exports.clickSearchButton = () => {
    searchButton.click();

    var isVisible = EC.visibilityOf(cardTitle);
    browser.wait(isVisible, 50000);
}

exports.validateCardTitle = (title) =>{
    cardTitle.getText().then(function(text){
        expect(text).to.equal(title);
    });
}
exports.validateCardContent = (content) =>{
    cardContent.getText().then(function(text){
        expect(text).to.equal(content);
    });
}
exports.validateCardFooter = (footer) =>{
    cardFooter.getText().then(function(text){
        expect(text).to.contain(footer);
    });
}
